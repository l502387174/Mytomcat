package http;

import java.io.IOException;

/**
 * 自定义Servlet
 *
 * @author 50238
 */
public class Servlet extends MyServlet {

    @Override
    void doGet(Request request, Response response) {
        try {
            response.write("doGet");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    void doPost(Request request, Response response) {
        try {
            response.write("doPost");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
