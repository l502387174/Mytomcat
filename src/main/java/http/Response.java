package http;

import java.io.IOException;
import java.io.OutputStream;

/**
 * 输出
 *
 * @author 50238
 */
public class Response {
    private OutputStream outputStream;

    static Response create(OutputStream outputStream) {
        return new Response(outputStream);
    }

    private Response(OutputStream outputStream) {
        this.outputStream = outputStream;
    }

    /**
     * 输出内容
     * 这里指定协议，数据类型，返回数据等
     *
     * @param outString
     */
    public void write(String outString) throws IOException {
        String httpResponse = "HTTP/1.1 200 OK\n" +
                "Content-type: text/html\n" +
                "\r\n" +
                "<html><body>" +
                outString +
                "</body></html>";
        outputStream.write(httpResponse.getBytes());
        outputStream.close();
    }
}
