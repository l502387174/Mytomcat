package http;


import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * request封装类
 *
 * @author 50238
 */
public class Request {

    static Request create(InputStream inputStream) {
        return new Request(inputStream);
    }

    /**
     * http
     */
    private String httpString;

    /**
     * 请求方式
     */
    private String method;

    /**
     * 请求地址
     */
    private String url;


    private Request(InputStream inputStream) {
        if (Objects.isNull(inputStream)) {
            throw new NullPointerException("http请求输入错误");
        }
        try {
            byte[] inputByte = new byte[1024];
            inputStream.read(inputByte);
            httpString = new String(inputByte);
            analysis();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 解析http
     * 主要根据http协议进行解析:header body参数等
     * 这里有一定感觉所谓的协议，不过是按照规矩组成的字符串
     * {参考http协议官方文档@link: https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Messages}
     */
    private void analysis() {
        String[] split = httpString.split("\\n");
        System.out.println(httpString);
        this.method = split[0].split("\\s")[0];
        this.url = split[0].split("\\s")[1];
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
