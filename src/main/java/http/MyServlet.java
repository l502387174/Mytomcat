package http;

/**
 * 自定义servlet
 *
 * @author 50238
 */
abstract class MyServlet {
    /**
     * get method
     *
     * @param request
     * @param response
     */
    abstract void doGet(Request request, Response response);

    /**
     * post method
     *
     * @param request
     * @param response
     */
    abstract void doPost(Request request, Response response);

    /**
     * 解析请求方式
     * 这里可以进行解析其他请求方式post get put delete等
     * {参考官方文档@link：https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Methods/DELETE}
     *
     * @param request
     * @param response
     */
    void analysis(Request request, Response response) {
        String method = request.getMethod().toUpperCase();
        switch (method) {
            case "GET":
                doGet(request, response);
                break;
            case "POST":
                doPost(request, response);
                break;
            default:
                throw new NullPointerException("请求方法不支持");
        }
    }
}
